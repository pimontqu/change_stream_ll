-- start of file --
CREATE EXTENSION ltree;

-- satements --
-- in create --
path ltree NOT NULL,
-- after create--
CREATE INDEX statements_path_idx ON statements USING GIST (path);

-- contents --
-- in create --
path ltree NOT NULL,
-- after create--
CREATE INDEX contents_path_idx ON contents USING GIST (path);