from insert_psql.bd import bd
# model pour la table users_association
class users_association(bd):
    def __init__(self, name = None):
        bd.__init__(self)
        self.id = None
        self.name = name
    
    # verifie s'il existe par son nom
    def exist(self):
        sql ="""SELECT COUNT(*) FROM users_association 
                WHERE name=%s"""
        cursor = self.connect.cursor()
        cursor.execute(sql, (self.name,))
        exist = cursor.fetchone()[0]
        cursor.close()
        return exist

    # ajoute un users_association et récupère son id
    def add(self):
            sql = """INSERT INTO users_association(name)
            VALUES(%s) RETURNING id"""
            cursor = self.set_cursor()
            cursor.execute(sql,(self.name,))
            self.id = cursor.fetchone()[0]
            self.connect.commit()
            cursor.close()
    
    # récupère l'id par son name
    def get(self):
        sql = """SELECT id FROM users_association
        WHERE name = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.name,))
        self.id = cursor.fetchone()[0]
        cursor.close()
