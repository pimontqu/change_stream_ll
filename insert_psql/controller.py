import uuid
import re
from insert_psql.contents import contents
from insert_psql.content_types import content_types
from insert_psql.periods import periods
from insert_psql.statements import statements
from insert_psql.users_association import users_association
from insert_psql.users import users
from insert_psql.verbs import verbs
from datetime import datetime
import pytz

# renseigne le path à un statement ou content
def create_path(obj):
    # rècupère le path du parent
    parent_path = obj.get_parent_path()
    # concatene le path parent et son id 
    return parent_path + '.' + str(obj.parent_id)

def set_url_statement(statement_in_mongo, is_limit_test = True):
    if(is_limit_test):
        url_statement = statement_in_mongo['context']['contextActivities']['other'][0]['id']
    else:
        grouping_activities = statement_in_mongo['context']['contextActivities']['grouping']
        url_statement = grouping_activities[len(grouping_activities) - 1]['id']
    return url_statement
# crée ou récupère le verbe
def set_verb(statement_in_mongo = None, verb_name = None, verb_id_url = None):
    # si la partie du statement du json de mongo n'est pas spécifié on utitlise le nom du verbe et son id_url spécifié
    if(statement_in_mongo is None):
        verb = verbs(name=verb_name, id_url=verb_id_url)
    # sinon on search le nom et l'id_url dans la partie du statement du json de mongo 
    else:
        verb = verbs(name=statement_in_mongo['verb']['display']['en'], id_url=statement_in_mongo['verb']['id'])
    # si le verb n'existe pas dans la bdd alors on l'ajoute
    if (not verb.exist()):
        verb.add()
    # sinon on récupère l'id du verb
    else:
        verb.get_verb()
    verb.close_connection()
    return verb

# annonymise l'utilisateur
def anonymization_user(user_association):
    # namespace obligatoire pour l'annonymisation
    uuid_namespace = uuid.UUID('4dd9f2cb-6198-5321-813c-0634ac12c9c6')
    # user annonymiseé
    uuid_user = uuid.uuid5(uuid_namespace, user_association.name)
    # retourne la uuid de l'user en string
    return str(uuid_user)

# créé ou récupère l'utilisateur anonymiser
def set_user_association(statement_in_mongo):
    # rechercerhe le nom de l'user dans la parie statement du json de mongo
    user_association = users_association(name=statement_in_mongo['actor']['name'])
    if(not user_association.exist()):
        user_association.add()
    else:
        user_association.get()
    return user_association

# créé ou récupère l'utilisateur anonymiser
def set_user(user_association):
    # instancie l'objet user avec l'id_user_association
    c_user = users(name=None, id_user_association=user_association.id)
    if(not c_user.exist()):
        # on anonymize le nom de l'user
        c_user.name = anonymization_user(user_association)
        c_user.add()
    else:
        c_user.get()
    return c_user

# crée un content par le json de mongo
def set_content_by_mongo(context_activity, position_to_end):
    # recupère la partie qui correspond au content voulu
    content_in_mongo = context_activity[len(context_activity) - position_to_end]
    # instancie un objet content avec le titre et l'url qui sont dans mongo
    content_by_mongo = contents(title=content_in_mongo['definition']['name']['en'], url=content_in_mongo['id'])
    return content_by_mongo

# crée ou récupère le content_type
def set_content_type(url_type = None, object_statement = None, name = None):
    # si le name est spécifié on l'assigne directement a la variable
    if(name is not None):
        content_type_name = name
    else:
        # sinon on transforme l'url_type en list pour chaque slash(/)
        url_type_split = url_type.split('/')
        # Puis on récupère la dernière valeur de la list pour avoir le nom du tpye
        content_type_name = url_type_split[len(url_type_split) - 1]
        # si le type est une interaction(Question) on recupère a la place le type de la question
        if(content_type_name =='cmi.interaction'):
            content_type_name = object_statement['definition']['interactionType']
    # instancie l'objet content_types avec le nom du type
    content_type = content_types(name=content_type_name)
    if(not content_type.exist()):
        content_type.add()
    else:
        content_type.get()
    return content_type

# crée ou récupère le parent du content
def set_parent_content(context_activity, verb = None, position_to_end=None):
    # si la position vers de la fin n'est pas spécifié
    if(position_to_end is None):
        # si c'est un statement de réponse de question la position de la fin est 2
        if(verb.name == 'answered'):
            position_to_end = 2
        # sinon la position de la fin est 1
        else:
            position_to_end = 1
    # si ma positon de la fin est specifié alors on lui ajoute 1
    else:
        position_to_end += 1
    # on crée l'objet content pour le parent
    parent_content = set_content_by_mongo(context_activity, position_to_end)
    if(not parent_content.exist()):
        # si le parent content n'existe pas on recupère son url type
        parent_url_type = context_activity[len(context_activity) - position_to_end]['definition']['type']
        # on instancie l'objet content_type pour lui
        parent_content_type = set_content_type(parent_url_type)
        # on lui assigne l'id de son type
        parent_content.id_content_types = parent_content_type.id
        # si la vers la fin est inférieur à la longueur de la liste context_activity on lui assigne son parent
        if(position_to_end < len(context_activity)):
            parent_content.parent_id = set_parent_content(context_activity=context_activity, position_to_end=position_to_end)
            parent_content.path = create_path(parent_content)
        # on l'enregiste dans la base
        parent_content.add()
    # sinon on récupère son id
    else:
        parent_content.get()
    parent_content.close_connection()
    return parent_content.id

# enregistre le min_score et le max_score du content
def set_min_max(content, statement_in_mongo):
    # assignation du min_score et max_score au content
    content.min_score = statement_in_mongo['result']['score']['min']
    content.max_score = statement_in_mongo['result']['score']['max']
    # enregistre le min et max
    content.set_min_max()

# crée le content pour un stament received (récap de test)
def set_content_received(statement_in_mongo):
    # liste des contetenu (chemin pour acceder à ce contenu)
    context_activity = statement_in_mongo['context']['contextActivities']['grouping']
    # position avant la fin de la liste
    position_to_end = 2
    # crée le content
    contents_received = set_content_by_mongo(context_activity, position_to_end)
    # url_type du content
    url_type = context_activity[len(context_activity) - position_to_end]['definition']['type']
    # on récupère de le type du content
    contents_received_type = set_content_type(url_type=url_type, object_statement=statement_in_mongo['object'])
    # on assigne l'id du content type au content
    contents_received.id_content_types = contents_received_type.id
    # on récupère le parent du content
    contents_received.parent_id = set_parent_content(context_activity,position_to_end=position_to_end)
    contents_received.path = create_path(contents_received)
    return contents_received

# crée ou récupère le content
def set_content(statement_in_mongo, verb, is_create_login = False, is_create_start = False, grouping_in_mongo = None, position_to_end = None):
    content_type = None
    # si c'est une création de login ou de start on récupère l'élément spécifié dans statement in mongo
    if(is_create_login or is_create_start):
        object_statement = statement_in_mongo
    # sinon on récupère le partie object du statement
    else:
        object_statement = statement_in_mongo['object']
    # si c'est un statement received 
    if(verb.name == 'received'):
        # on crée le content received
        content = set_content_received(statement_in_mongo)
        # on initialise url_type avec Nonz
        url_type = None
    else:
        # sinon on crée l'objet contents avec les infos de la partie object du json mongo
        content = contents(title=object_statement['definition']['name']['en'], url=object_statement['id'])
        # récupère l'url type
        url_type = object_statement['definition']['type']
    # si le content est different de attemp
    if(content.title != 'Attempt'):
        # si le url type n'est pas none
        if(url_type is not None):
            # récupère le content type
            content_type = set_content_type(url_type, object_statement)
            # assigne le type au content
            content.id_content_types = content_type.id
            # si une créatrion de start test ou que ce n'est pas une creation de login et que grouping et dans le context_activity
            if(is_create_start or not is_create_login and 'grouping' in statement_in_mongo['context']['contextActivities']):
                # si une créatrion de start test alors context activity est assigné avec grouping
                if(is_create_start):
                    context_activity = grouping_in_mongo
                # sinon on le récupère directement dans le mongodb
                else:
                    context_activity = statement_in_mongo['context']['contextActivities']['grouping']
                content.parent_id = set_parent_content(context_activity, verb, position_to_end=position_to_end)
                content.path = create_path(content)
        if(not content.exist()):
            content.add()
        else:
            content.get()
        # si c'est un statement de completion de test on ajoute le min et max score
        if(verb.name == 'completed' and content_type.name == 'assessment'):
            set_min_max(content, statement_in_mongo)
        return content
    else:
        print('c\'est exclus le attemps')
        content.close_connection()
        return False

# récupère le score d'un test ou de question
def set_scorescaled(statement_in_mongo, is_completed=True):
    # si c'est une completion de test on récupère le screscaled directement dans le mongodb
    if(is_completed):
        scorescaled = statement_in_mongo['result']['score']['scaled']
    else:
        # sinon si le champs success est dans le mongodb alors on le recupère sinon on l'initialise 0
        if('success' in statement_in_mongo['result']):
            success_answer = statement_in_mongo['result']['success']
            # si la question est réussi on met le score à 1 sinon on le met à 0
            if(success_answer):
                scorescaled = 1
            else:
                scorescaled = 0
        else:
            scorescaled = None
    return scorescaled

# crée un statement d'une reponse à une question
def set_response_statement(statement, content_id, response = None):
    # si la réponse est spécifié on la récupère au format string
    if(response is not None) :
        response = str(response)
    # on crée un staments avec le parent, l'user, data(réponse a la question), et le content
    response_statement = statements(parent_id=statement.id, id_users=statement.id_users, data=response, id_contents=content_id, url=statement.url)
    response_statement.path = create_path(response_statement)
    # on enrgistre le statement
    response_statement.add()
    response_statement.close_connection()
    return response_statement

# assigne la duration du test
def set_duration_test(statement_in_mongo):
    # récupère la duration au format xapi (PT00S)
    duration_test_in_mongo = statement_in_mongo['result']['duration']
    # pattern pour extraire le nombre de secondes
    pattern = '([0-9]+)'
    # extraction du temps en second du test
    duration_test_catch = re.search(pattern, duration_test_in_mongo)
    duration_test = duration_test_catch.group(1)
    return duration_test

# crée un login inexistant
def create_login_missing(user, start, statement_in_mongo):
    # création d'un verbe login
    login_verb = set_verb(verb_name='logged into', verb_id_url='https://brindlewaye.com/xAPITerms/verbs/loggedin/')
    # récupèration du content dans le mongodb
    content_in_mongo = statement_in_mongo['context']['contextActivities']['grouping'][0]
    # création ou récupération du content
    content_login = set_content(content_in_mongo, login_verb, True)
    # creation du statement de login
    login_statement = statements(start=start, id_verbs=login_verb.id, id_users=user.id, id_contents=content_login.id)
    # sauvegarde du statement
    login_statement.add()
    # fermeture des connexion
    login_statement.close_connection()
    login_verb.close_connection()
    content_login.close_connection()

# crée un start test inexistant
def create_start_missing(user, start, statement_in_mongo):
    # création ou récupération du verb
    start_verb = set_verb(verb_name='started', verb_id_url='http://activitystrea.ms/schema/1.0/start')
    # grouping dans le mongo
    grouping_in_mongo = statement_in_mongo['context']['contextActivities']['grouping']
    # poisition de la fin du grouping
    position_to_end = 2
    # content dans mongodb
    content_in_mongo = grouping_in_mongo[len(grouping_in_mongo) - position_to_end]
    # création ou récupèration du content
    start_content = set_content(content_in_mongo, start_verb, False, True, grouping_in_mongo, position_to_end)
    # création du statement
    start_statement = statements(start=start, id_verbs=start_verb.id, id_users=user.id, id_contents=start_content.id)
    # recupération du parent
    start_statement.get_parent_connection()
    if(start_statement.parent_id is None):
        create_login_missing(user, start, statement_in_mongo)
        start_statement.get_parent_connection()
    start_statement.path = create_path(start_statement)
    # fermeture des connexion
    start_statement.close_connection()
    start_verb.close_connection()
    start_content.close_connection()

# creation de statment
def set_statement(verb, user, start, content, statement_in_mongo):
    print(verb.name)
    # indique si c'est une réponse de question
    is_answer = False
    # si c'est une completion de test
    if(verb.name == 'completed'):
        # création du statement
        statement = statements(parent_id=None, start=None, end=start, id_verbs=verb.id, id_users=user.id, id_contents=content.id, scorescaled= set_scorescaled(statement_in_mongo))
        statement.url = set_url_statement(statement_in_mongo)
        # recupèration de l'heure de debut du test
        statement.get_last_start_test()
        # recupèration du parent
        statement.get_parent_test()
        if(statement.parent_id is None):
            create_start_missing(user, start, statement_in_mongo)
            statement.get_parent_test()
        statement.path = create_path(statement)
        # récupèration de la durée des test
        statement.duration_test = set_duration_test(statement_in_mongo)
    else:
        # sinon on cré le statement
        statement = statements(parent_id=None, start=start, end=None, id_verbs=verb.id, id_users=user.id, id_contents=content.id)
        if(verb.name != 'logged into' and verb.name != 'registered to'):
            # si ce n'est pas un login ni une inscription alors on récupère le dernier statement de l'utilisiteur
            last_statement = statements()
            last_statement.get_last_statement(statement.id_users)
            # si le dernier statement n'est pas nul ni un login on lui assigne son heure de fin
            if(not last_statement.is_login() and last_statement.id is not None):
                last_statement.set_end(statement.start)
            last_statement.close_connection()
            # c'est une réponse a une question ou un recap de test
            if(verb.name == 'answered' or verb.name == 'received'):
                statement.url = set_url_statement(statement_in_mongo, False)
                # on récupère le test parent
                statement.get_parent_test()
                if(statement.parent_id is None):
                    create_start_missing(user, start, statement_in_mongo)
                    statement.get_parent_test()
                statement.path = create_path(statement)
                # si c'est un ereponse on definis le score scaled est on passe la variable is_answer à true
                if(verb.name == 'answered'):
                    statement.scorescaled = set_scorescaled(statement_in_mongo, is_completed=False)
                    is_answer = True
            # sinon on récupère la dernière connexion de l'user
            else:
                if(verb.name == 'started'):
                    statement.url = set_url_statement(statement_in_mongo)
                statement.get_parent_connection()
                if(statement.parent_id is None):
                    create_login_missing(user, start, statement_in_mongo)
                    statement.get_parent_connection()
                statement.path = create_path(statement)
    print('current statement : ' + user.name + ' à/se ' + verb.name + ' le/aux ' + content.title + ' à ' + str(start))
    ##### Debug #####
    if(statement.parent_id != None):
        debug_statement = statements()
        debug_parent_statement = debug_statement.get_statement_by_id(statement.parent_id)
        print('parent statement : id : ' + str(debug_parent_statement['id']) + ' verb : ' + debug_parent_statement['verb'] + ' content : ' + debug_parent_statement['content'])
        debug_statement.close_connection()
    if(content.parent_id != None):
        debug_content = contents()
        debug_parent_content = debug_content.get_by_id(content.parent_id)
        print('parent content : id : ' + str(debug_parent_content['id']) + ' title : ' + debug_parent_content['title'] + ' type : ' + debug_parent_content['type'])
        debug_content.close_connection()
    ##### Debug #####
    # ajout du statement
    statement.add()
    if(is_answer):
        # si c'est une response on récupère son type
        content_type = content_types()
        content_type.get_by_content_id(statement.id_contents)
        response_type = set_content_type(name='response')
        # si c'est une réponse a une question vrai-faux, résponse courte ou numérique
        if(content_type.name == 'true-false' or content_type.name == 'fill-in' or content_type.name == 'numeric'):
            # on récupère la réponse et on enregistre le statement
            response = statement_in_mongo['result']['response']
            response_statement = set_response_statement(statement, statement.id_contents, response=response)
        # si c'est un qcu/qcm
        if(content_type.name == 'choice'):
            # on récupère la/les réponse(s) de l'utilisateur
            responses = statement_in_mongo['result']['response']
            # on fait une liste avec toute les réponses
            responses_list = responses.split('[,]')
            # pour chaque réponse
            for response in responses_list:
                # creation du content pour la response
                response_content = contents(title=None, url=None, text=response, min_score=None, max_score=None, id_content_types=response_type.id, parent_id=statement.id_contents)
                response_content.path = create_path(response_content)
                # s'il elle n'existe pas on la sauvegarde dans la base sinon on le récupère son id
                if(not response_content.exit_response_content()):
                    ### a tester sans le add_response_content
                    response_content.add()
                    ### a tester sans le add_response_content
                else:
                    response_content.get_response_content()
                # on crée le statement
                response_statement = set_response_statement(statement, response_content.id)
                response_content.close_connection()
    return statement

# récupère le timestamp de mongodb et le convertit dans la bonne timezone
def set_start(doc_mongo):
    # format de la date
    bdd_format = '%Y-%m-%d %H:%M:%S'
    # nouvelle timezone 
    local_tz = 'Europe/Paris'
    # recupèration du timestamp dans mongo (timezone utc)
    timestamp_utc = doc_mongo['timestamp']
    timestamp_utc = timestamp_utc.replace(tzinfo=pytz.utc)
    # on convertit le timetamps utc dans la bonne timezone et au format de la bdd
    timestamp = timestamp_utc.astimezone(pytz.timezone(local_tz))
    timestamp_bdd_format = timestamp.strftime(bdd_format)
    return timestamp_bdd_format

# Fonction qui apelle tous les autres du fichier
def convert_mongo_to_psql(doc_mongo):
    # staements dans mongo
    statement_in_mongo = doc_mongo['statement']
    # récupèration du start
    start = set_start(doc_mongo)
    # récupèration du verbe
    verb = set_verb(statement_in_mongo)
    # récupèration du content
    content = set_content(statement_in_mongo, verb)
    # si le content exist
    if(content):
        # récupèration du user assoc
        user_assoc = set_user_association(statement_in_mongo)
        # récupèration du  user
        user = set_user(user_assoc)
        # récupèration du statement
        statement = set_statement(verb, user, start, content, statement_in_mongo)
        content.close_connection()
        user_assoc.close_connection()
        user.close_connection()
        statement.close_connection()
    verb.close_connection()