import psycopg2
from configparser import ConfigParser
# class mère pour les models
class bd :

    def __init__(self, filename='db.ini', section='postgresql'):
        # objet qui permet de lire le db.ini
        parser = ConfigParser()
        # lit le fichier .ini
        parser.read(filename)
        # récupèra les champs du fichier ini
        db_config = {}
        # si la section postgresql exist
        if parser.has_section(section):
            # récupère la section
            params = parser.items(section)
            # pour chaque param dans la section
            for param in params:
                # on ajoute au dictionnaire le nom du param en clé et sa valeur en valeur
                db_config[param[0]] = param[1]
        else:
            # sinon on indique que la section n'existe pas
            raise Exception ('Section {0} not found in the {1} file'.format(section, filename))
        # objet qui permettra le communication avec la bdd
        self.connect = None
        try:
            # on essaye de se connecter avec les paramètres de fichier.ini
            self.connect = psycopg2.connect(**db_config)
        except (Exception, psycopg2.DatabaseError) as error:
            # si la connexion echoue on affiche l'erreur de l'échec
            print(error)

    # crée un cursor qui perment d'executer des requête sql
    def set_cursor(self):
        return self.connect.cursor()

    # ferme la connexion avec la bdd
    def close_connection(self):
        if self.connect is not None :
            self.connect.close()