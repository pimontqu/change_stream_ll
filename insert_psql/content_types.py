from insert_psql.bd import bd
# Model pour la table content_types
class content_types(bd):
    def __init__(self, name = None):
        # ouvre la conexion avec la bdd
        bd.__init__(self)
        # champs id 
        self.id = None
        # champ name
        self.name = name

    # ajoute un content type
    def add(self):
        # requête sql
        sql = """INSERT INTO content_types(name)
        VALUES(%s) RETURNING id"""
        # crée le cursor
        cursor = self.set_cursor()
        # execute la requête avec les paramètre
        cursor.execute(sql,(self.name,))
        # Récupère l'id de l'élément ajouté
        self.id = cursor.fetchone()[0]
        # sauvegarde les changment dans la bdd
        self.connect.commit()
        # ferme le cursor
        cursor.close()
    
    # verifie si le content_types par son nom
    def exist(self):
        sql = """SELECT COUNT(*) FROM content_types
        WHERE name = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.name,))
        # recupère la valeur 1 s'il existe ou 0 sinon
        exist = cursor.fetchone()[0]
        cursor.close()
        return exist
    
    # Récupère l'id par son nom
    def get(self):
        sql = """SELECT id FROM content_types
        WHERE name = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.name,))
        self.id = cursor.fetchone()[0]
        cursor.close()

    # récupère le nom et l'id d'un content_type par l'id du content
    def get_by_content_id(self, id):
        sql = """SELECT content_types.id, content_types.name FROM contents
        INNER JOIN content_types ON content_types.id = contents.id_content_types
        WHERE contents.id = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(id,))
        query_response = cursor.fetchone()
        self.id = query_response[0]
        self.name = query_response[1]
        cursor.close()