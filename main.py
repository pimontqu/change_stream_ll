import os
import pymongo
import datetime
import pytz
from bson.json_util import dumps
from insert_psql import controller
client = pymongo.MongoClient('mongodb://localhost:27017')



change_stream = client.learninglocker_v2.statements.watch([{
    '$match' : {
        'operationType': {'$in' : ['insert']}
    }
}])
for entry in change_stream:
    doc_mongo = entry['fullDocument']
    print('################################################################################')
    print('')
    print(dumps(doc_mongo))
    print('')
    
    controller.convert_mongo_to_psql(doc_mongo)
    
    print('')
    print('################################################################################')
    print('')