-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2
-- PostgreSQL version: 10.0
-- Project Site: pgmodeler.io
-- Model Author: ---


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: userrelation | type: DATABASE --
-- -- DROP DATABASE IF EXISTS userrelation;
-- CREATE DATABASE userrelation
-- 	ALLOW_CONNECTIONS = false;
-- -- ddl-end --
-- 

-- object: bdd_learning_analitycs | type: SCHEMA --
-- DROP SCHEMA IF EXISTS bdd_learning_analitycs CASCADE;
CREATE SCHEMA bdd_learning_analitycs;
-- ddl-end --
-- ALTER SCHEMA bdd_learning_analitycs OWNER TO postgres;
-- ddl-end --

SET search_path TO pg_catalog,public,bdd_learning_analitycs;
-- ddl-end --
CREATE EXTENSION ltree;

-- object: public.users | type: TABLE --
-- DROP TABLE IF EXISTS public.users CASCADE;
CREATE TABLE public.users (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	name varchar(50) NOT NULL,
	id_users_association bigint,
	CONSTRAINT users_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.users OWNER TO postgres;
-- ddl-end --

-- object: public.users_association | type: TABLE --
-- DROP TABLE IF EXISTS public.users_association CASCADE;
CREATE TABLE public.users_association (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	name varchar(50) NOT NULL,
	CONSTRAINT users_association_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.users_association OWNER TO postgres;
-- ddl-end --

-- object: public.statements | type: TABLE --
-- DROP TABLE IF EXISTS public.statements CASCADE;
CREATE TABLE public.statements (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ,
	parent_id bigint,
	start timestamp,
	"end" timestamp,
	duration_test int2,
	url varchar(255),
	scorescaled float,
	data varchar(255),
	path ltree NOT NULL,
	id_verbs bigint,
	id_users bigint NOT NULL,
	id_contents bigint,
	CONSTRAINT statements_pk PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE public.statements IS E'An event, as you would find in xAPI, enriched with :\n* an end time when the event is only relevant for a particular duration. (i.e. the student connect to the platform, the statement stay relevent until he disconnects)\n* potentially nested : while the student stay connected, he take a quiz, and in this quiz he chose a particular set of answers';
-- ddl-end --
-- ALTER TABLE public.statements OWNER TO postgres;
-- ddl-end --
CREATE INDEX statements_path_idx ON statements USING GIST (path);

-- object: public.contents | type: TABLE --
-- DROP TABLE IF EXISTS public.contents CASCADE;
CREATE TABLE public.contents (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	parent_id bigint,
	title varchar(100),
	url varchar(1024),
	text varchar,
	min_score integer,
	max_score integer,
	path ltree NOT NULL,
	id_content_types integer NOT NULL,
	CONSTRAINT id_pk_cp PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE public.contents IS E'Any kind of content or it''s container, nested : a platform usualy containts course category, then courses, sections, quiz, questions and choices';
-- ddl-end --
-- ALTER TABLE public.contents OWNER TO postgres;
-- ddl-end --
CREATE INDEX contents_path_idx ON contents USING GIST (path);

-- object: users_association_fk | type: CONSTRAINT --
-- ALTER TABLE public.users DROP CONSTRAINT IF EXISTS users_association_fk CASCADE;
ALTER TABLE public.users ADD CONSTRAINT users_association_fk FOREIGN KEY (id_users_association)
REFERENCES public.users_association (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: users_uq | type: CONSTRAINT --
-- ALTER TABLE public.users DROP CONSTRAINT IF EXISTS users_uq CASCADE;
ALTER TABLE public.users ADD CONSTRAINT users_uq UNIQUE (id_users_association);
-- ddl-end --

-- object: users_fk | type: CONSTRAINT --
-- ALTER TABLE public.statements DROP CONSTRAINT IF EXISTS users_fk CASCADE;
ALTER TABLE public.statements ADD CONSTRAINT users_fk FOREIGN KEY (id_users)
REFERENCES public.users (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: contents_fk | type: CONSTRAINT --
-- ALTER TABLE public.statements DROP CONSTRAINT IF EXISTS contents_fk CASCADE;
ALTER TABLE public.statements ADD CONSTRAINT contents_fk FOREIGN KEY (id_contents)
REFERENCES public.contents (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.content_types | type: TABLE --
-- DROP TABLE IF EXISTS public.content_types CASCADE;
CREATE TABLE public.content_types (
	id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	name varchar(25),
	CONSTRAINT content_types_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.content_types OWNER TO postgres;
-- ddl-end --

-- object: content_types_fk | type: CONSTRAINT --
-- ALTER TABLE public.contents DROP CONSTRAINT IF EXISTS content_types_fk CASCADE;
ALTER TABLE public.contents ADD CONSTRAINT content_types_fk FOREIGN KEY (id_content_types)
REFERENCES public.content_types (id) MATCH FULL
ON DELETE RESTRICT ON UPDATE CASCADE;
-- ddl-end --

-- object: public.verbs | type: TABLE --
-- DROP TABLE IF EXISTS public.verbs CASCADE;
CREATE TABLE public.verbs (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	name varchar(25) NOT NULL,
	id_url varchar(255) NOT NULL,
	CONSTRAINT verbs_pk PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE public.verbs IS E'the type of action that was run. i.e: connected, experienced, attempted';
-- ddl-end --
-- ALTER TABLE public.verbs OWNER TO postgres;
-- ddl-end --

-- object: verbs_fk | type: CONSTRAINT --
-- ALTER TABLE public.statements DROP CONSTRAINT IF EXISTS verbs_fk CASCADE;
ALTER TABLE public.statements ADD CONSTRAINT verbs_fk FOREIGN KEY (id_verbs)
REFERENCES public.verbs (id) MATCH FULL
ON DELETE SET NULL ON UPDATE CASCADE;
-- ddl-end --

-- object: public.periods | type: TABLE --
-- DROP TABLE IF EXISTS public.periods CASCADE;
CREATE TABLE public.periods (
	id bigint NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT BY 1 MINVALUE 0 MAXVALUE 2147483647 START WITH 1 CACHE 1 ),
	start timestamp NOT NULL,
	"end" timestamp NOT NULL,
	name varchar(100) NOT NULL,
	CONSTRAINT periods_pk PRIMARY KEY (id)

);
-- ddl-end --
-- ALTER TABLE public.periods OWNER TO postgres;
-- ddl-end --

-- object: statements_recurse_fk | type: CONSTRAINT --
-- ALTER TABLE public.statements DROP CONSTRAINT IF EXISTS statements_recurse_fk CASCADE;
ALTER TABLE public.statements ADD CONSTRAINT statements_recurse_fk FOREIGN KEY (parent_id)
REFERENCES public.statements (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --

-- object: contents_recurse_fk | type: CONSTRAINT --
-- ALTER TABLE public.contents DROP CONSTRAINT IF EXISTS contents_recurse_fk CASCADE;
ALTER TABLE public.contents ADD CONSTRAINT contents_recurse_fk FOREIGN KEY (parent_id)
REFERENCES public.contents (id) MATCH FULL
ON DELETE CASCADE ON UPDATE CASCADE;
-- ddl-end --


