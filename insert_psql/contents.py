from insert_psql.bd import bd
# model de la table contents
class contents(bd):
    def __init__(self, title = None, url = None, text = None, min_score = None, max_score = None, id_content_types = None, parent_id = None, path = 'root') :
        bd.__init__(self)
        # champs id
        self.id = None
        # champs de la table
        # champs qui represent le parent dans le chemin du moodle
        self.parent_id = parent_id
        self.title = title
        self.url = url
        self.text = text
        self.path = path
        self.min_score = min_score
        self.max_score = max_score
        self.id_content_types = id_content_types

    # verifie si le content exist par son url
    def exist(self) :
        sql = """SELECT COUNT(*) FROM contents 
        WHERE url = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.url,))
        exist = cursor.fetchone()[0]
        cursor.close()
        return exist

    # ajoute un contente et retourne son id
    def add(self) :
        sql = """INSERT INTO contents(parent_id, title, url, text, min_score, max_score, id_content_types, path)
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s) RETURNING id"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.parent_id, self.title, self.url, self.text, self.min_score, self.max_score, self.id_content_types, self.path))
        self.id = cursor.fetchone()[0]
        self.connect.commit()
        cursor.close()

    # récupère l'id par son url
    def get(self):
        sql = """SELECT id FROM contents
        WHERE url = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.url,))
        self.id = cursor.fetchone()[0]
        cursor.close()
    
    # vérifie content qui a pour role une reponse a une question
    def exit_response_content(self):
        sql = """SELECT COUNT(*) FROM contents
        WHERE parent_id = %s AND text = %s AND id_content_types = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.parent_id, self.text, self.id_content_types))
        cursor.fetchone()[0]
        cursor.close()

    # récupère un content qui a pour role une reponse a une question par l'id de la question et le text de la réponse
    def get_response_content(self):
        sql = """SELECT id FROM contents
        WHERE text = %s AND parent_id = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.text, self.parent_id))
        self.id = cursor.fetchone()[0]
        cursor.close()
        
    # récupère le titre et le name de son content_typê par son id
    def get_by_id(self, id):
        sql = """SELECT contents.title, content_types.name FROM contents
        INNER JOIN content_types ON content_types.id = contents.id_content_types
        WHERE contents.id = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql, (id,))
        response_request = cursor.fetchone()
        cursor.close()
        return {'id': id, 'title': response_request[0], 'type': response_request[1]}
    
    # Ajoute le min_score et le max_score par l'id du content
    def set_min_max(self):
        sql = """UPDATE contents
        SET min_score = %s, max_score = %s
        WHERE id = %s and min_score is null and max_score is null"""
        cursor = self.set_cursor()
        print(self.id)
        print(self.max_score)
        print(self.min_score)
        cursor.execute(sql, (self.min_score, self.max_score, self.id))
        self.connect.commit()
        cursor.close()

    # récupère le path du parent
    def get_parent_path(self):
        sql = """SELECT path FROM contents 
        WHERE id = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.parent_id,))
        parent_path = cursor.fetchone()[0]
        cursor.close()
        return parent_path