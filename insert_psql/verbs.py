from insert_psql.bd import bd
# model pour la table verbs
class verbs(bd):
    def __init__(self, name = None, id_url = None):
        bd.__init__(self)
        self.id = None
        self.name = name
        self.id_url = id_url
    
    # verifie si le verbs existe par son id_url
    def exist(self):
        sql = """SELECT COUNT(*) FROM verbs WHERE id_url = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.id_url,))
        exist = cursor.fetchone()[0]
        cursor.close()
        return exist

    # récupère l'id par son id_url
    def get_verb(self) :
        sql = """SELECT id FROM verbs WHERE id_url = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.id_url,))
        self.id = cursor.fetchone()[0]
        cursor.close()

    # ajoute un verbs et récupère son id
    def add(self) :
        sql = """INSERT INTO verbs(name, id_url)
        VALUES(%s, %s) RETURNING id"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.name, self.id_url))
        self.id = cursor.fetchone()[0]
        self.connect.commit()
        cursor.close()
