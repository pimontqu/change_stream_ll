from insert_psql.bd import bd
# model pour la table users
class users(bd):
    def __init__(self, name = None, id_user_association = None):
        bd.__init__(self)
        self.id  = None
        self.name = name
        self.id_user_association = id_user_association

    # verifie si l'user exit par l'id users association
    def exist(self) :
        sql = """SELECT COUNT(*) from users WHERE id_users_association = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.id_user_association,))
        exist = cursor.fetchone()[0]
        cursor.close()
        return exist

    # ajoute un user et récupère son id
    def add(self) :
        sql = """INSERT INTO users (name, id_users_association)
        VALUES(%s, %s) RETURNING id"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.name, self.id_user_association))
        self.id = cursor.fetchone()[0]
        self.connect.commit()
        cursor.close()

    # récupère le nom et l'id par son id users association
    def get(self) :
        sql = """SELECT id, name FROM users
        WHERE id_users_association = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.id_user_association,))
        fetch = cursor.fetchone()
        print(fetch)
        self.id = fetch[0]
        self.name = fetch[1]
        print(self.name)
        cursor.close()

    