from insert_psql.bd import bd
# model pour la table statements
class statements(bd):
    def __init__(self, parent_id = None, start = None, end = None, id_verbs = None, id_users = None, id_contents = None, scorescaled = None, data = None, duration_test = None, path = 'root', url = None):
        bd.__init__(self)
        # champs de la table
        self.id = None
        # le nom du verb
        self._verb_name = None
        self.parent_id = parent_id
        self.start = start
        self.end = end
        self.path = path
        self.id_verbs = id_verbs
        self.id_users = id_users
        self.id_contents = id_contents
        self.scorescaled = scorescaled
        self.data = data
        self.duration_test = duration_test
        self.url = url

    # ajoute un statements et récupère son id
    def add(self):
        sql = """INSERT INTO statements(parent_id, start, "end", id_verbs, id_users, id_contents, scorescaled, data, duration_test, path, url)
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id"""
        cursor = self.set_cursor()
        cursor.execute(sql,(self.parent_id, self.start, self.end, self.id_verbs, self.id_users, self.id_contents, self.scorescaled, self.data, self.duration_test, self.path, self.url))
        self.id = cursor.fetchone()[0]
        self.connect.commit()
        cursor.close()

    # recupère l'id du dernier statements de connexion de cet utilisateur
    def get_parent_connection(self):
        sql = """SELECT statements.id FROM statements 
        INNER JOIN verbs ON verbs.id = statements.id_verbs
        WHERE verbs.name = 'logged into' AND statements.id_users = %s
        ORDER BY statements.start DESC
        LIMIT 1"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.id_users,))
        returned_values = cursor.fetchone()
        if(returned_values is not None):
            # on assigne la valeur a l'id seulement si la connection exit
            self.parent_id = returned_values[0]
        cursor.close()

    # récupère l'id du dernier statement de l'utilasateur
    def get_last_statement(self, user):
        select_sql = """SELECT statements.id, verbs.name FROM statements 
        INNER JOIN verbs ON verbs.id = statements.id_verbs
        WHERE statements.id_users = %s
        ORDER BY statements.start DESC, statements.id
        LIMIT 1"""
        cursor = self.set_cursor()        
        cursor.execute(select_sql, (user,))
        returned_values = cursor.fetchone()
        if(returned_values is not None):
            self.id = returned_values[0]
            self._verb_name = returned_values[1]
        cursor.close()
    
    # enrgistre l'heure de fin du statement
    def set_end(self, end):    
        update_sql = """UPDATE statements
        SET "end" = %s
        WHERE id = %s"""
        cursor = self.set_cursor()
        cursor.execute(update_sql, (end, self.id))
        self.connect.commit()
        cursor.close()
    
    # récupère le heure de début du test
    def get_last_start_test(self):
        sql = """SELECT statements.start FROM statements
        INNER JOIN verbs ON verbs.id = statements.id_verbs
        WHERE statements.id_users = %s AND  verbs.name = 'started' AND statements.id_contents = %s AND statements.url = %s
        ORDER BY statements.start DESC
        LIMIT 1"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.id_users, self.id_contents, self.url))
        returned_values = cursor.fetchone()
        if(returned_values is not None):
            self.start = returned_values[0]
        cursor.close()

    # récupère l'id de l'évènement de commencement du test 
    def get_parent_test(self):
        sql = """SELECT statements.id FROM statements
        INNER JOIN verbs ON verbs.id = statements.id_verbs
        WHERE statements.id_users = %s AND  verbs.name = 'started' AND statements.url = %s
        ORDER BY statements.start DESC
        LIMIT 1"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.id_users, self.url))
        returned_values = cursor.fetchone()
        if(returned_values is not None): 
            self.parent_id = returned_values[0]
        cursor.close()

    # Vérifie si c'est un statement de login
    def is_login(self):
        if(self._verb_name == 'logged into'):
            return True
        else :
            return False
    
    # récupère le nom du verb et le titre du content du statements
    def get_statement_by_id(self, id):
        sql = """SELECT verbs.name, contents.title FROM statements
        INNER JOIN verbs ON verbs.id = statements.id_verbs
        INNER JOIN contents ON contents.id = statements.id_contents
        WHERE statements.id = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql, (id,))
        request_result = cursor.fetchone()
        cursor.close()
        return {'id': id, 'verb': request_result[0], 'content': request_result[1]}

    # récupère le path du parent
    def get_parent_path(self):
        sql = """SELECT path FROM statements 
        WHERE id = %s"""
        cursor = self.set_cursor()
        cursor.execute(sql, (self.parent_id,))
        parent_path = cursor.fetchone()[0]
        cursor.close()
        return parent_path